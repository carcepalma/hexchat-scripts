import hexchat
import win32gui
import win32process
import psutil

COMANDO = "me escuchando:"
TITULO = ""
SWAG = "[Hex + Spotify]"

__module_name__ = "Spotichet"
__module_version__ = "0.2"
__module_description__ = "Spotichet para Wintendo"
__author__ = "Mshggh"

hexchat.prnt("Cargaste el script \002{}\002 version \002{}\002 para Windows. Por \002{}\002".format(__module_name__, __module_version__, __author__))
hexchat.prnt("El comando para ejecutar el script es: \002/np\002")

def get_spotify_pids():
    pids = []
    for proc in psutil.process_iter(['pid', 'name']):
        if 'Spotify' in proc.info['name']:  # Busca cualquier proceso que contenga "Spotify"
            pids.append(proc.info['pid'])
    return pids

def find_spotify_window():
    spotify_pids = get_spotify_pids()
    if not spotify_pids:
        print("Spotify no está ejecutándose.")
        return None

    def enum_windows_callback(hwnd, pid_list):
        # Obtiene PID de la ventana
        tid, window_pid = win32process.GetWindowThreadProcessId(hwnd)
        if window_pid in spotify_pids and win32gui.IsWindowVisible(hwnd):
            pid_list.append(hwnd)

    spotify_windows = []
    win32gui.EnumWindows(enum_windows_callback, spotify_windows)

    # Retorna la primera ventana si esta existe
    if spotify_windows:
        return spotify_windows[0]  # Retorna el primer handle de ventana encontrada
    else:
        print("Parecer ser que no se Spotify no ha sido ejecutado.")
        return None
        
def spotify(word, worde_ol, userdata):
    hwnd = find_spotify_window()
    if hwnd and win32gui.GetWindowText(hwnd) not in "Spotify Premium":
        window_title = win32gui.GetWindowText(hwnd)
        hexchat.command(f'{COMANDO} \002{window_title}\002 {SWAG}')
    else:
        hexchat.prnt("Spotify no está reproduciendo nada.")

def unload_cb(userdata):
    hexchat.prnt("Descargaste el script \002{}\002 para Windows.".format(__module_name__,))

hexchat.hook_command("np", spotify, help="/np")
hexchat.hook_unload(unload_cb)