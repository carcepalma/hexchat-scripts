from __future__ import print_function

import hexchat
import win32gui

COMANDO = "me escuchando:"
SWAG = "[Hex + Spotify]"

__module_name__ = "Spotichet"
__module_version__ = "0.1"
__module_description__ = "Spotichet para Wintendo"
__author__ = "Mshggh"

hexchat.prnt("Cargaste el script \002{}\002 version \002{}\002 para Windows. Por \002{}\002".format(__module_name__, __module_version__, __author__))
hexchat.prnt("El comando para ejecutar el script es: \002/np\002")

def spotify(word, wordeol, userdata):
    hwnd = win32gui.FindWindow("Chrome_WidgetWin_0", None)
    if not hwnd:
        hexchat.prnt("Spotify no se está ejecutando.")
        return hexchat.EAT_ALL

    song_and_artist = win32gui.GetWindowText(hwnd)

    if not song_and_artist:
        hexchat.prnt("No hay nada sonando.")
        return hexchat.EAT_ALL
    if song_and_artist == "Spotify Premium":
        hexchat.prnt("No hay nada sonando.")
        return hexchat.EAT_ALL

    if song_and_artist.startswith("Spotify - "):
        song_and_artist = song_and_artist[:len("Spotify - ")]

    final_text = "{} \002{}\002 {}".format(COMANDO, song_and_artist, SWAG)

    hexchat.command(final_text)
    return hexchat.EAT_ALL

def unload_cb(userdata):
    hexchat.prnt("Descargaste el script \002{}\002 para Windows.".format(__module_name__,))

hexchat.hook_command("np", spotify, help="/np")
hexchat.hook_unload(unload_cb)
