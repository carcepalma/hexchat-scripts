from __future__ import print_function
from random import choice
from tabulate import tabulate
import hexchat

__module_name__ = 'context'
__module_version__ = '0.1'
__module_description__ = 'context testing'
__author__ = 'Mshggh'

def context_cb(word, word_eol, userdata):
	hexchat.prnt(word[1])
	cnc = hexchat.find_context(channel='{}'.format(word[1]))
	hexchat.prnt(cnc.get_info("nick"))
	for u in cnc.get_list("users"):
		host = u.host.split("@")
		print("Nick: \002{}\002 <===> Ident: \002{}\002 <===> Host: \002{}\002".format(u.nick, host[0], host[1]))
		database = open("info.txt", "a")
		database.write("{} {}\n".format(host[1], u.nick))
		database.close()

def unload_cb(userdata):
    print(__module_name__, 'version', __module_version__, 'ha sido descargado.')

hexchat.hook_command('context', context_cb, help='context #sala')
hexchat.hook_unload(unload_cb)
print(__module_name__, 'version', __module_version__, "creado por\002", __author__,'\002ha sido cargado.')