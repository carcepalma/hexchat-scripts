from __future__ import print_function
from random import choice

import hexchat

__module_name__ = 'lanzar'
__module_version__ = '0.1'
__module_description__ = 'Segundo script para Hexchat'
__author__ = 'Mshggh'

objetos = [
    'chair',
    'table',
    'rock',
    'Coca Cola can',
    'human head',
    'vodka',
    'piscola'
]

def lanzar_cb(word, word_eol, userdata):
    if len(word) > 1:
        hexchat.command("me throws a \002{}\002 at \002{}\002".format(choice(objetos), word[1]))
    else:
        hexchat.command('help lanzar')
    return hexchat.EAT_ALL

def unload_cb(userdata):
    print(__module_name__, 'version', __module_version__, 'ha sido descargado.')

hexchat.hook_command('lanzar', lanzar_cb, help='lanzar <nick>')
hexchat.hook_unload(unload_cb)
print(__module_name__, 'version', __module_version__, "creado por\002", __author__,'\002ha sido cargado.')
